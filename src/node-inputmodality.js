//Author: Debkanya Mazumder <dmazum2@uic.edu>

"use strict";

var util = require('util');
var sageutils = require('./node-utils');
var Kinect2 = require('kinect2');
var Omicron = require('./node-omicron');
var HttpServer = require('./node-httpserver');       // creates web server
var InteractableManager = require('./node-interactable');     // handles geometry and determining which object a point is over
var Interaction = require('./node-interaction');      // handles sage interaction (move, resize, etc.)
var Loader = require('./node-itemloader');       // handles sage item creation
var Drawing = require('./node-drawing');          // handles Omicron input events
var Radialmenu = require('./node-radialmenu');       // radial menu
var Sage2ItemList = require('./node-sage2itemlist');    // list of SAGE2 items
var Sagepointer = require('./node-sagepointer');      // handles sage pointers (creation, location, etc.)
var StickyItems = require('./node-stickyitems');
var registry = require('./node-registry');        // Registry Manager

var PartitionList = require('./node-partitionlist');		// list of SAGE2 Partitions
var inputModalityManager;

function InputModalityManager(deviceType, sysConfig) {

    inputModalityManager = this; 
    this.device = null;
    this.deviceType = deviceType;

    //For Touch 
    this.config = undefined;
    this.touchOffset = [0, 0];
    this.totalWidth = 0;
    this.totalHeight = 0;

    // Default Gestures
    this.enableDoubleClickMaximize = true;
    this.enableThreeFingerRightClick = true;
    this.enableTwoFingerWindowDrag = true;
    this.enableTwoFingerZoom = true;
    this.enableFiveFingerCloseApp = false;

    if (sysConfig.experimental !== undefined) {
        this.config = sysConfig.experimental.omicron;
    }

    // Default config
    if (this.config === undefined) {
        this.config = {};
        this.config.enable = false;
        this.config.dataPort = 30005;
        this.config.eventDebug = false;

        this.config.zoomGestureScale = 2000;
        this.config.acceleratedDragScale = 3;
        this.config.gestureDebug = false;

        this.config.msgPort = 28000;
    }

    //deviceType 1 - Touchless Input via Kinect. 
    //deviceType 2 - Touch Input 
    //deviceType 3 - Vicon Tracker
    if (deviceType == 1) {
        this.device = new Kinect2();
    }
    else if(deviceType == 2 || deviceType == 3)
    {
        //access node-omicron.js for touch and vicon input 
        this.device = new Omicron(sysConfig);
    }

}

InputModalityManager.prototype.connect = function (portnumber) {

    const dgram = require('dgram');
    const server = dgram.createSocket('udp4');

    server.on('error', (err) => {
        console.log(`server error:\n${err.stack}`);
        server.close();
    });

    server.on('message', (msg, rinfo) => {
        console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`);
        inputModalityManager.processIncomingEvent(msg, rinfo);
    });

    server.on('listening', () => {
        var address = server.address();
        console.log(`server listening ${address.address}:${address.port}`);
    });

    server.bind(portnumber);
    // server listening 0.0.0.0:41234

}


InputModalityManager.prototype.processIncomingEvents = function(msg,rinfo) { 

    var dstart = Date.now();
    var emit = 0;
    this.nonCriticalEventDelay = -1;
    this.lastNonCritEventTime = dstart;

    /*
	if(rinfo == undefined) {
		console.log(sageutils.header('Omicron') + "incoming TCP");
	} else {
		console.log(sageutils.header('Omicron') + "incoming UDP");
	}
	*/
    var offset = 0;
    var e = {};
    if (offset < msg.length) {
        e.timestamp = msg.readUInt32LE(offset); offset += 4;
    }
    if (offset < msg.length) {
        e.sourceId = msg.readUInt32LE(offset); offset += 4;
    }
    if (offset < msg.length) {
        e.serviceId = msg.readInt32LE(offset); offset += 4;
    }
    if (offset < msg.length) {
        e.serviceType = msg.readUInt32LE(offset); offset += 4;
    }
    if (offset < msg.length) {
        e.type = msg.readUInt32LE(offset); offset += 4;
    }
    if (offset < msg.length) {
        e.flags = msg.readUInt32LE(offset); offset += 4;
    }
    if (offset < msg.length) {
        e.posx = msg.readFloatLE(offset); offset += 4;
    }
    if (offset < msg.length) {
        e.posy = msg.readFloatLE(offset); offset += 4;
    }
    if (offset < msg.length) {
        e.posz = msg.readFloatLE(offset); offset += 4;
    }
    if (offset < msg.length) {
        e.orw = msg.readFloatLE(offset); offset += 4;
    }
    if (offset < msg.length) {
        e.orx = msg.readFloatLE(offset); offset += 4;
    }
    if (offset < msg.length) {
        e.ory = msg.readFloatLE(offset); offset += 4;
    }
    if (offset < msg.length) {
        e.orz = msg.readFloatLE(offset); offset += 4;
    }
    if (offset < msg.length) {
        e.extraDataType = msg.readUInt32LE(offset); offset += 4;
    }
    if (offset < msg.length) {
        e.extraDataItems = msg.readUInt32LE(offset); offset += 4;
    }
    if (offset < msg.length) {
        e.extraDataMask = msg.readUInt32LE(offset); offset += 4;
    }
    // Extra data types:
    //    0 ExtraDataNull,
    //    1 ExtraDataFloatArray,
    //    2 ExtraDataIntArray,
    //    3 ExtraDataVector3Array,
    //    4 ExtraDataString,
    //    5 ExtraDataKinectSpeech
    if (e.extraDataType == 0) {
        e.extraDataSize = 0;
    } else if (e.extraDataType == 1 || e.extraDataType == 2) {
        e.extraDataSize = e.extraDataItems * 4;
    } else if (e.extraDataType == 3) {
        e.extraDataSize = e.extraDataItems * 4 * 3;
    } else if (e.extraDataType == 4) {
        e.extraDataSize = e.extraDataItems;
    } else if (e.extraDataType == 5) {
        e.extraDataSize = e.extraDataItems;
    }

    // var r_roll  = Math.asin(2.0*e.orx*e.ory + 2.0*e.orz*e.orw);
    // var r_yaw   = Math.atan2(2.0*e.ory*e.orw-2.0*e.orx*e.orz , 1.0 - 2.0*e.ory*e.ory - 2.0*e.orz*e.orz);
    // var r_pitch = Math.atan2(2.0*e.orx*e.orw-2.0*e.ory*e.orz , 1.0 - 2.0*e.orx*e.orx - 2.0*e.orz*e.orz);
    var posX = e.posx * inputModalityManager.totalWidth;
    var posY = e.posy * inputModalityManager.totalHeight;
    posX += inputModalityManager.touchOffset[0];
    posY += inputModalityManager.touchOffset[1];

    var sourceID = e.sourceId;

    // serviceType:
    // 0 = Pointer
    // 1 = Mocap
    // 2 = Keyboard
    // 3 = Controller
    // 4 = UI
    // 5 = Generic
    // 6 = Brain
    // 7 = Wand
    // 8 = Speech
    // 9 = Ipad Framework
    var serviceType = e.serviceType;
    // console.log("Event service type: " + serviceType);

    // console.log(e.sourceId, e.posx, e.posy, e.posz);
    // serviceID:
    // (Note: this depends on the order the services are specified on the server)
    // 0 = Touch
    // 1 = Classic SAGEPointer
    // var serviceID = e.serviceId;

    // Appending sourceID to pointer address ID
    var address = sourceID;
    if (rinfo !== undefined) {
        address = rinfo.address + ":" + sourceID;
    } else {
        address = inputModalityManager.config.inputServerIP + ":" + sourceID;
    }
    console.log("reading mocap data");
    // ServiceTypePointer
    //
    if (serviceType === 0) {
        omicronManager.processPointerEvent(e, sourceID, posX, posY, msg, offset, address, emit, dstart);
    }
    else if (serviceType === 1) {
        console.log("Mocap Position: ("+e.posx+", "+e.posy+","+e.posz+")" );
        console.log("Mocap Rotation: ("+e.orx+", "+e.ory+","+e.orz+","+e.orw+")" );
    }
    
}; 

InputModalityManager.prototype.setDeviceOn = function () {
    //deviceType 1 - Touchless Input via Kinect. 
    //deviceType 2 - Touch Input
    if (this.deviceType == 1) {
        if (this.device.open()) {
            console.log(sageutils.header('Input-Modality') + "Kinect Opened");
            //listen for body frames
            this.device.on('bodyFrame', function (bodyFrame) {
                for (var i = 0; i < bodyFrame.bodies.length; i++) {
                    if (bodyFrame.bodies[i].tracked) {
                        console.log(sageutils.header('Input-Modality') + util.inspect(bodyFrame.bodies[i], { showHidden: false, depth: null }));
                        console.log(sageutils.header('Input-Modality') + util.inspect(bodyFrame.bodycount, { showHidden: false, depth: null }));
                    }
                }
            });

            //request body frames
            this.device.openBodyReader();
        }
        else {
            console.log(sageutils.header('Input-Modality') + "Kinect is not opened");
        }
    }
    else if (this.deviceType == 2 || this.deviceType == 3) {

        //this.device.connect(this.config.msgPort);
        //this.device.runTracker();
    }

};

InputModalityManager.prototype.setDeviceOff = function () {
    if (this.deviceType == 1) {
        this.device.close();
        console.log(sageutils.header('Input-Modality') + "Kinect Closed");
    }
    else if (this.deviceType == 2 || this.deviceType == 3) {
        //Touch Based Input
        this.device.disconnect();

    }
};

//To call this constructor from server.js 
module.exports = InputModalityManager;