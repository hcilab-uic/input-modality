
"use strict";

var inputModalities = SAGE2_App.extend({
    init:function(data){
        this.SAGE2Init("div", data);

        this.resizeEvents = "continuous"; // "onfinish";

        this.element.id = "div" + data.id;

        // force this div to have specified width and height
        // var workingDiv = this.element;
        // workingDiv.width  = this.element.clientWidth  + "px";
        // workingDiv.height = this.element.clientHeight + "px";

        // use up the entire amount such that a scaling will occur when the app is increased / decreased in size.
        this.drawCanvas = document.createElement('canvas');
        this.drawCanvas.id           = this.element.id + "DrawCanvas";
        this.drawCanvas.width        = data.width;
        this.drawCanvas.height       = data.height;
        this.drawCanvas.style.width  = "100%";
        this.drawCanvas.style.height = "100%";
        this.element.appendChild(this.drawCanvas);
        this.ctx = this.drawCanvas.getContext("2d");
        this.ctx.fillStyle = "#FFFFFF";
        this.ctx.fillRect(0, 0, this.drawCanvas.width, this.drawCanvas.height);
        this.ctx.fillStyle = "#000000";

        this.log("Init"); 
    },
    load: function(date) {
        console.log('articulate_ui> Load with state value', this.state.value);

        this.refresh(date);
    },
    event:function(eventType, position, user_id, data, date) { 
        if ( eventType === "kinectInput"){
            this.load; 
            this.log("kinect input running"); 
        }


    }, 
    draw:function(date){
        this.log("Draw"); 
    },
    resize:function(date){
        this.refresh(date);
    },
    move:function(date){
        this.refresh(date);
    },
    
    event: function (eventType, position, user_id, data, date) {
        if(eventType == "kinect")
       
    },
    quit: function () {
        this.log("Done"); 
    }
    });