/**
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

// DO NOT EDIT THIS GENERATED OUTPUT DIRECTLY!
// This file should be overwritten as part of your build process.
// If you need to extend the behavior of the generated service worker, the best approach is to write
// additional code and include it using the importScripts option:
//   https://github.com/GoogleChrome/sw-precache#importscripts-arraystring
//
// Alternatively, it's possible to make changes to the underlying template file and then use that as the
// new base for generating output, via the templateFilePath option:
//   https://github.com/GoogleChrome/sw-precache#templatefilepath-string
//
// If you go that route, make sure that whenever you update your sw-precache dependency, you reconcile any
// changes made to this original template file with your modified copy.

// This generated service worker JavaScript will precache your site's resources.
// The code needs to be saved in a .js file at the top-level of your site, and registered
// from your pages in order to be used. See
// https://github.com/googlechrome/sw-precache/blob/master/demo/app/js/service-worker-registration.js
// for an example of how you can register this script and handle various service worker events.

/* eslint-env worker, serviceworker */
/* eslint-disable indent, no-unused-vars, no-multiple-empty-lines, max-nested-callbacks, space-before-function-paren, quotes, comma-spacing */
'use strict';

var precacheConfig = [["/css/Arimo-Bold.woff","d5eef9ec661fcd757f1964707f378f19"],["/css/Arimo-Bold.woff2","18d25d912119f8201e583610e58a23e1"],["/css/Arimo-BoldItalic.woff","e54bc3b46c49523ff6787f25e29256c4"],["/css/Arimo-BoldItalic.woff2","78fc8e10269404deb171b332be284f01"],["/css/Arimo-Italic.woff","b50288bfb7a7ab13fc9558db6292826a"],["/css/Arimo-Italic.woff2","7df47337b63a6f3d31fcc90fb938aac5"],["/css/Arimo-Regular.woff","4e494348a608592eaab9ff8ad9a0e929"],["/css/Arimo-Regular.woff2","b055dc4438ee9edf0bc67a7a36ca24cc"],["/css/dialog-polyfill.css","3a8f210f668d1cab3d1c922f86e7d28d"],["/css/fileManager.css","640d3170ada947c7c7cfd656b24b5360"],["/css/fonts.css","3535faff6d95989af6e3c13ce36d255e"],["/css/style.css","ac9ece5bb567962d8efccfcbc2cba5f0"],["/css/style_audio.css","b28bb10e04ebcf6c01679962765cc666"],["/css/style_mgr.css","bf9997d5e4768535a6f23cf36eca8ee4"],["/css/style_pointer.css","ce1406c3125d08aad20e4edb5e4813c3"],["/css/style_ui.css","83c7a534898ec3eb589c0b578bca1cbc"],["/favicon.ico","56769358cd129f024966e18e446bc118"],["/images/EVL-LAVA.svg","7025fc02b124dcee8ec659be102f0a6d"],["/images/EVL-LAVA_UI.svg","2ccd2203535712ce280e5195958abe3a"],["/images/EVL-logo.svg","42bc74e43aa8a1a8ef2b3b2d236613be"],["/images/LAVA-logo.svg","5e477a41abe51d370ec710353aeca518"],["/images/SAGE2 Application Interaction.svg","3d45d09bc1db7e8e732609ca03207dc5"],["/images/SAGE2 Pointer Arrow.svg","df9f1affa5d7a1f01ae57c76957ec180"],["/images/SAGE2 Window Manipulation.svg","c6e4affddd0883c9c09550bce5c8fc13"],["/images/SAGE2Pointer-help.svg","143f88260ad67ff8a25c84590d21aba1"],["/images/appUi/arrowDownBtn.svg","f2d26ea7e55c1e7352f7ed6507f956c6"],["/images/appUi/arrowLeftBtn.svg","12f17172524ce9de7d729c9c666a0b5e"],["/images/appUi/arrowRightBtn.svg","09e4189147bf8936d65c152833ba15a0"],["/images/appUi/arrowUpBtn.svg","ce762ca8f8b104def6fca2f3f73992ba"],["/images/appUi/closeAppBtn.svg","d422abfae31b38e85814aaaedeb4d73e"],["/images/appUi/closeMenuBtn.svg","bd0117b4e78ec61a00ef07b04c24470e"],["/images/appUi/directionsBtn.svg","dee155f5edf00510e5e86206b9d71cd9"],["/images/appUi/dontLoopBtn.svg","969b8dc19805e7b8234f796fef9f5a31"],["/images/appUi/endBtn.svg","c5773efd78632c776f53adbd4f4b8c56"],["/images/appUi/homeBtn.svg","b156bc864b85eb3cb35e9cc7248b345c"],["/images/appUi/loopBtn.svg","823af62655dd942e3fe2eb1f51752b41"],["/images/appUi/muteBtn.svg","d1124ee7310b3b0c273e2509b8dbf540"],["/images/appUi/nextBtn.svg","c8761e391ffed308fb1449c2134a5d98"],["/images/appUi/pauseBtn.svg","a1d76b19c9d4acc3439cba60f15a2706"],["/images/appUi/playBtn.svg","df941061732eb53cace62e33859fdff4"],["/images/appUi/playPauseBtn.svg","b1806d577fea6ad3d436cdbf9864e060"],["/images/appUi/previousBtn.svg","0d5749b0dd3342379278b2f69dc485e9"],["/images/appUi/soundBtn.svg","a6f642c46c0cde21cf0e8e17ec0cc0f8"],["/images/appUi/stickyBtn.svg","0c1aae386a2a7d78e289f7e96bfcd68f"],["/images/appUi/stickyCopyBtn.svg","0bf1d5ac722d9df30b59a355804506b3"],["/images/appUi/stopBtn.svg","5c05b4afb008905629b80bc3b529ed89"],["/images/appUi/trafficBtn.svg","48d4032ae4a767fcebe43abcef2f6606"],["/images/appUi/weatherBtn.svg","30226826aa53c96811489809ade4911a"],["/images/appUi/zoomInBtn.svg","410a7f29eb67d9e2d63a28ac82682cd6"],["/images/appUi/zoomOutBtn.svg","e90c92c92dbc744787ec8e6eb08206a7"],["/images/blank.png","bae3a0c7a44ee3eafab36f5f14702977"],["/images/closemobilepointer.svg","879011370b917656da85acb3926b9bdd"],["/images/dir-closed.svg","2d3373ecc788a54da55fd67c9a4d6422"],["/images/dir-open.svg","0f4f61b400b7745c73720871058816df"],["/images/layout3.svg","18512a35ac7d3b7e6d16ca7514d76cc7"],["/images/mouse-left.svg","7ebaaf48a7069d3515e8655e0d6c6eaf"],["/images/mouse-right.svg","9cc0ffc6a92b06eab759145f0fba7fb1"],["/images/radialMenu/adobe20.svg","e25a2cee537acc5d733b29d629e76fda"],["/images/radialMenu/camera.svg","c617407024c7f496bce27153b0823876"],["/images/radialMenu/camera2.svg","fea203d7b2798367c688dce12bc4d44f"],["/images/radialMenu/checkbox-partial.svg","8fa2498043c945d98b62304b0eb34439"],["/images/radialMenu/checkbox-unchecked.svg","12d911356e99956151ebe9051e243037"],["/images/radialMenu/clapper.svg","0c09de4d9a9e78ad351561ea6f787e4c"],["/images/radialMenu/cog.svg","c6da589800f3628c85cd0008ece7d061"],["/images/radialMenu/disk.svg","ea9e1db1e736f85882f9253b1bfaf784"],["/images/radialMenu/download.svg","66450a38a449a0b81018464ad8e3bc56"],["/images/radialMenu/download2.svg","98301bd888c65093017d0775e916b6be"],["/images/radialMenu/drag-ring.svg","8441c6c79e6c4b2cab7ae15adeee99e6"],["/images/radialMenu/drag-ring2.svg","7ae5f887a355a3909497b6311bba0acb"],["/images/radialMenu/earth.svg","2cf175c85dc344f4a977cfda22662bc8"],["/images/radialMenu/exit.svg","ae777a68a1df53f4a24d8be10c2fdfea"],["/images/radialMenu/file-excel.svg","bc797138f8e821b7f04991f06d216bd9"],["/images/radialMenu/file-openoffice.svg","03ab9b5a4229e51a43d60a9913ae3c44"],["/images/radialMenu/file-pdf.svg","af8601b14f96012d6231c5c2f5315139"],["/images/radialMenu/file-powerpoint.svg","bb39c364d85aca6790430ca27e161f2b"],["/images/radialMenu/file-word.svg","4db7016fde9e292a2171d6e8514f3553"],["/images/radialMenu/file-zip.svg","1f2f956397b491b4bbac39c0924d3ffd"],["/images/radialMenu/file.svg","562eb0d86d80ff6a6e95b9a7f689c6f3"],["/images/radialMenu/film.svg","d41be985cd8de66939150e937f50b0a3"],["/images/radialMenu/folder-open.svg","1905c4ded29cb95444d997640f31737f"],["/images/radialMenu/folder.svg","605837b5126d83a60fa5b02b6e41ebbf"],["/images/radialMenu/four62.svg","0f14cb95f5617c76bb378fc85f5fab40"],["/images/radialMenu/gears3.svg","352836856e3a85d00e01a9af6f44df7b"],["/images/radialMenu/globe.svg","8016459d0675eaab0849945ffedb8d21"],["/images/radialMenu/glow-line.svg","9afa4933a12ae4da8d035b82acf3423a"],["/images/radialMenu/icon_radial_button_circle.svg","b6200e238c1511eeac01519cf949f3f2"],["/images/radialMenu/image.svg","59897403d03320e7c89f9f9374c52693"],["/images/radialMenu/image2.svg","b48573cc520201317b1915084a49b690"],["/images/radialMenu/images.svg","ac33d03078055147e371c754876186cf"],["/images/radialMenu/keyboard50.svg","8fa92ab4c6e3cdf99f5a2205466a42e8"],["/images/radialMenu/large16.svg","c37da6a93e97142f685f835f9c7a2adb"],["/images/radialMenu/lock.svg","1aafc33c2ad05dc4d6af5dbcc3d82796"],["/images/radialMenu/maximize.svg","0cad964fc629b5b9df64d7488b48b607"],["/images/radialMenu/open131.svg","79c1ef89dedf17e241e466649f2a0c47"],["/images/radialMenu/pdf19.svg","449fa7fff2b036d9f0a3f8c6756b8629"],["/images/radialMenu/play.svg","8361bd5e37f634bc0f295ffae7463e8f"],["/images/radialMenu/radio-checked.svg","e29cf9b0349abaa6241200bd3619ab05"],["/images/radialMenu/radio-unchecked.svg","2ebbfc1455d52e9a856d8273d74b6a38"],["/images/radialMenu/rocket.svg","73d092c45297725f6a3f9c661649f4bf"],["/images/radialMenu/switch.svg","00e0884d464c97d11b21f39f89194b10"],["/images/radialMenu/three115.svg","7ca720924246c976fbfc47414985e985"],["/images/radialMenu/tools6.svg","ff048b756ccd432826e98564e783df27"],["/images/radialMenu/tree.svg","8dcb2d7bed614f28ba93e74d4cb75ade"],["/images/radialMenu/unlocked.svg","2bf94ee983af8fa47236b0c292b03924"],["/images/radialMenu/upload.svg","50a5b003913e84b097454fdd8f7b1054"],["/images/radialMenu/upload2.svg","386999cbd2542e971f5363c9fe9cccff"],["/images/radialMenu/window27.svg","f57e0f6f3db03f08a6da909d4541b0fb"],["/images/radialMenu/windows24.svg","47093df2db564d94ab089974ad897d7c"],["/images/radialMenu/wrench.svg","4cacc4ba5e4705e43abdc262f12fe675"],["/images/sage2.svg","7496195321d58e15f6cd9a72d43e7a11"],["/images/spacebar.svg","4b84649e9aa905f7c1a7ca884e1a561d"],["/images/switch.svg","a7d9bb179828b390134cca47d89a648c"],["/images/trackpad.svg","87f399e479850d9f7a9d729b6b02fa05"],["/images/ui/admin.svg","763c25590ccc27dd1a87bd7b881e4aee"],["/images/ui/applauncher.svg","b6e6550641e8217b225cdb6c1704a254"],["/images/ui/arrangement.svg","9fc945e0ed12a880a0e705c383e4ef65"],["/images/ui/browser.svg","595922ed0f157b8d95f9bf9ee18f56df"],["/images/ui/clearcontent.svg","28fdd05a0702bee27174054869063a26"],["/images/ui/close.svg","fcbdb8d25d8bba422d6fa92028fba5e7"],["/images/ui/graycircle-admin.svg","06999a7dac69a7f207327bdc4f164051"],["/images/ui/graycircle-applauncher.svg","28bc61a46433b1458ef53d998cd0d795"],["/images/ui/graycircle-arrangement.svg","82bcc1ef576441506c7acf78fc592709"],["/images/ui/graycircle-assigntopartitions.svg","ea3a7d216511e48fa33b924973d40b22"],["/images/ui/graycircle-browser.svg","7cf6a8cd5578fb187fb6800401cedc73"],["/images/ui/graycircle-clearcontent.svg","ae7dde6ba5cbff4ddfe6317d0a69d101"],["/images/ui/graycircle-close.svg","e4a015cbc89c2c0f076b6efce6a1512b"],["/images/ui/graycircle-createpartitions.svg","c83a31086e6bf54ca28f8a07645f3166"],["/images/ui/graycircle-deletepartitions.svg","a2600f570168819db4c254f801f6ae91"],["/images/ui/graycircle-ezDraw.svg","6dfeab64a988e043059777bea16efc6a"],["/images/ui/graycircle-ezNote.svg","cbe844baa7ee0e283843b864ff6676fa"],["/images/ui/graycircle-help.svg","01473261b2c3540a2adf968f3526986e"],["/images/ui/graycircle-images.svg","8091e3f296f59ff26a40fe17aad0ade9"],["/images/ui/graycircle-info.svg","ffee677944ebde71360246926e87bc31"],["/images/ui/graycircle-loadsession.svg","a24349ad9d07c288d354bd335db311d2"],["/images/ui/graycircle-mediabrowser.svg","9bf2b182e94a94d85569498d03395103"],["/images/ui/graycircle-pdfs.svg","940dc9e6c5194c2d8f9852fde8ed2267"],["/images/ui/graycircle-remote.svg","17472bbd34f37913f658fa114c14a092"],["/images/ui/graycircle-sage2pointer.svg","f25b4460f23e2ca3c35a494da008aaf3"],["/images/ui/graycircle-savesession.svg","31cd88d275454556fd0d975aaa477290"],["/images/ui/graycircle-settings.svg","ceeedf0c0494d9b242ff0baaa3191976"],["/images/ui/graycircle-sharescreen.svg","4d1a70ac34f8e02fe843832dd7650a5a"],["/images/ui/graycircle-tilecontent.svg","48d1464a374f3f874b40e25a302c543d"],["/images/ui/graycircle-videos.svg","5ba7c10e68a45ad7cb9f4dbb233a7889"],["/images/ui/graycircle-webview.svg","7cec01be7f920dcb4cba30b9ccdd046c"],["/images/ui/help.svg","870e0055eca231054a80897a5ac735b0"],["/images/ui/images.svg","6080ece4380bca141c77cdea288b7279"],["/images/ui/info.svg","da302eac4bd3cb4200210c76ff7d11a9"],["/images/ui/loadsession.svg","afc03842566557559f3fc0e49a1c72a2"],["/images/ui/mediabrowser.svg","c0451b60c4dab48bb1c69a15b1fc7f7d"],["/images/ui/pdfs.svg","8861da435e6ad7410c32f6cd7695393c"],["/images/ui/remote.svg","54277ae29e964e0a1e2e8ebaacf3d80f"],["/images/ui/sage2pointer.svg","65ccd81bba38811520ef0a2a67a1cf62"],["/images/ui/savesession.svg","363c0adc407541b783936e58beaf08c7"],["/images/ui/settings.svg","0ae7159fe706a4abe1e2fbfc0d8fb5d3"],["/images/ui/sharescreen.svg","96eacadd866f5d31f4c510d0d78465d8"],["/images/ui/tilecontent.svg","e3ae1653e1349382e9c88edbf3020c95"],["/images/ui/videos.svg","84a55a13be04663fbe9aa4ff0a874961"],["/images/window-close.svg","b93d4accd175176f1516fe282f880d26"],["/images/window-close2.svg","6777042d4501b9cc3419db1890f6f5d5"],["/images/window-close3.svg","ce5935a34ed0b098a1bc40fa7a7714ff"],["/images/window-fullscreen.svg","000d13381124887492474fb6d4afba09"],["/images/window-sync.svg","9a0920fcdd01bf10a86ad0c28a51baa4"],["/images/window-unsync.svg","30de4edc5b9110329bfa938d9a71d5cc"],["/lib/moment.min.js","0a8c0ed69de37d65b29e9e0de39e1eaa"],["/lib/webix/skins/compact.css","873723ec26bca2baa39c892112b2be43"],["/lib/webix/webix.css","5c1d4744212b17f75a7b3b4023282f1f"],["/lib/webix/webix.js","d62cd19d8b3e94a468c0b2bfa16e3eed"],["/src/Class.js","b21e187f2981360f50d7c1847c9c01f5"],["/src/DynamicImage.js","cd31d1bd21766eebb316ba57c9c54089"],["/src/SAGE2_App.js","6526f0ffe8244bc07f5e42213c886e28"],["/src/SAGE2_AudioManager.js","c8a1540159fb5002ea1c3b73622daba5"],["/src/SAGE2_BlockStreamingApp.js","404afe3d14adcfc3b5544d2b3bd97eb0"],["/src/SAGE2_DataSharing.js","9daae8d1a417bc6c7384f5a59cb56d6a"],["/src/SAGE2_Display.js","178ee84db58482f50034b37b04d769d9"],["/src/SAGE2_DisplayUI.js","cc40da68619010f58fa8c86aa4574525"],["/src/SAGE2_DrawingApp.js","5176bbe392479377de0706aa6ca875b2"],["/src/SAGE2_MouseEventPassing.js","bf3d7f8d4fbcfb027022a66b7c8ce705"],["/src/SAGE2_Partition.js","91bbfa2fc1938ce26bf32abc518c2948"],["/src/SAGE2_Session.js","977c9a5b8bf05928b9d091ba5e2def61"],["/src/SAGE2_UI.js","5f8eb1b2fa3b47a58950e47dc76c86df"],["/src/SAGE2_WebGLApp.js","b3c84487581dece1a064b6ea3eaf83d9"],["/src/SAGE2_WidgetButtonTypes.js","cd6d6ea107d9760666e67a3f398a9c26"],["/src/SAGE2_WidgetControl.js","686cc689cc1fdb3ed35a527109ffd7b2"],["/src/SAGE2_WidgetControlInstance.js","37be5e02a4db2e460ad8a4127181d4f3"],["/src/SAGE2_fileManager.js","791186712827ba2497033e0f866d8c7a"],["/src/SAGE2_interaction.js","87c49c271aa3a332d1f38d258aa514bf"],["/src/SAGE2_runtime.js","f62a510fa032349a01168eaac8419784"],["/src/image_viewer.js","09563cfef531536a98e7133d5e312563"],["/src/jupyter.js","37485949e5a404d36c6d1383e846a3c4"],["/src/md5.js","9c6586eccb4ceecca7b4ec322882a668"],["/src/media_block_stream.js","5bb530c68fd6d61375b04e81d85b4124"],["/src/media_stream.js","3068874eab84333f6d5c17e4af9ea83c"],["/src/movie_player.js","20976cc50f4547c2603104b73f5ac2de"],["/src/pdf_viewer.js","7f9012497c7cd7cd8aecbeb30683de11"],["/src/pdf_viewer_single.js","7fe32b10ab1b188e0bd4548c1cfa2fd2"],["/src/pointer.js","8e7b8d27ee194de8acd96ce153c376ba"],["/src/radialMenu.js","d8bb84bdb06a01c128fbe24fea39d1c3"],["/src/sagePointer.js","1a8e3e3ee354012b9af5c01aeb5d64fb"],["/src/text.js","45262b4819629c8c83a1d0a957dbd4c1"],["/src/ui_builder.js","c180c57e5a4303022b2a920a3995cd5d"],["/src/websocket.io.js","acd833243b017c1326d68cd7b756fa49"],["/src/widgetHelperFunctions.js","19dadf2a0bd6f3d7dd141f4b19007486"]];
var cacheName = 'sw-precache-v3-SAGE2-' + (self.registration ? self.registration.scope : '');


var ignoreUrlParametersMatching = [/^utm_/];



var addDirectoryIndex = function (originalUrl, index) {
    var url = new URL(originalUrl);
    if (url.pathname.slice(-1) === '/') {
      url.pathname += index;
    }
    return url.toString();
  };

var cleanResponse = function (originalResponse) {
    // If this is not a redirected response, then we don't have to do anything.
    if (!originalResponse.redirected) {
      return Promise.resolve(originalResponse);
    }

    // Firefox 50 and below doesn't support the Response.body stream, so we may
    // need to read the entire body to memory as a Blob.
    var bodyPromise = 'body' in originalResponse ?
      Promise.resolve(originalResponse.body) :
      originalResponse.blob();

    return bodyPromise.then(function(body) {
      // new Response() is happy when passed either a stream or a Blob.
      return new Response(body, {
        headers: originalResponse.headers,
        status: originalResponse.status,
        statusText: originalResponse.statusText
      });
    });
  };

var createCacheKey = function (originalUrl, paramName, paramValue,
                           dontCacheBustUrlsMatching) {
    // Create a new URL object to avoid modifying originalUrl.
    var url = new URL(originalUrl);

    // If dontCacheBustUrlsMatching is not set, or if we don't have a match,
    // then add in the extra cache-busting URL parameter.
    if (!dontCacheBustUrlsMatching ||
        !(url.pathname.match(dontCacheBustUrlsMatching))) {
      url.search += (url.search ? '&' : '') +
        encodeURIComponent(paramName) + '=' + encodeURIComponent(paramValue);
    }

    return url.toString();
  };

var isPathWhitelisted = function (whitelist, absoluteUrlString) {
    // If the whitelist is empty, then consider all URLs to be whitelisted.
    if (whitelist.length === 0) {
      return true;
    }

    // Otherwise compare each path regex to the path of the URL passed in.
    var path = (new URL(absoluteUrlString)).pathname;
    return whitelist.some(function(whitelistedPathRegex) {
      return path.match(whitelistedPathRegex);
    });
  };

var stripIgnoredUrlParameters = function (originalUrl,
    ignoreUrlParametersMatching) {
    var url = new URL(originalUrl);

    url.search = url.search.slice(1) // Exclude initial '?'
      .split('&') // Split into an array of 'key=value' strings
      .map(function(kv) {
        return kv.split('='); // Split each 'key=value' string into a [key, value] array
      })
      .filter(function(kv) {
        return ignoreUrlParametersMatching.every(function(ignoredRegex) {
          return !ignoredRegex.test(kv[0]); // Return true iff the key doesn't match any of the regexes.
        });
      })
      .map(function(kv) {
        return kv.join('='); // Join each [key, value] array into a 'key=value' string
      })
      .join('&'); // Join the array of 'key=value' strings into a string with '&' in between each

    return url.toString();
  };


var hashParamName = '_sw-precache';
var urlsToCacheKeys = new Map(
  precacheConfig.map(function(item) {
    var relativeUrl = item[0];
    var hash = item[1];
    var absoluteUrl = new URL(relativeUrl, self.location);
    var cacheKey = createCacheKey(absoluteUrl, hashParamName, hash, false);
    return [absoluteUrl.toString(), cacheKey];
  })
);

function setOfCachedUrls(cache) {
  return cache.keys().then(function(requests) {
    return requests.map(function(request) {
      return request.url;
    });
  }).then(function(urls) {
    return new Set(urls);
  });
}

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return setOfCachedUrls(cache).then(function(cachedUrls) {
        return Promise.all(
          Array.from(urlsToCacheKeys.values()).map(function(cacheKey) {
            // If we don't have a key matching url in the cache already, add it.
            if (!cachedUrls.has(cacheKey)) {
              var request = new Request(cacheKey, {credentials: 'same-origin'});
              return fetch(request).then(function(response) {
                // Bail out of installation unless we get back a 200 OK for
                // every request.
                if (!response.ok) {
                  throw new Error('Request for ' + cacheKey + ' returned a ' +
                    'response with status ' + response.status);
                }

                return cleanResponse(response).then(function(responseToCache) {
                  return cache.put(cacheKey, responseToCache);
                });
              });
            }
          })
        );
      });
    }).then(function() {
      
      // Force the SW to transition from installing -> active state
      return self.skipWaiting();
      
    })
  );
});

self.addEventListener('activate', function(event) {
  var setOfExpectedUrls = new Set(urlsToCacheKeys.values());

  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.keys().then(function(existingRequests) {
        return Promise.all(
          existingRequests.map(function(existingRequest) {
            if (!setOfExpectedUrls.has(existingRequest.url)) {
              return cache.delete(existingRequest);
            }
          })
        );
      });
    }).then(function() {
      
      return self.clients.claim();
      
    })
  );
});


self.addEventListener('fetch', function(event) {
  if (event.request.method === 'GET') {
    // Should we call event.respondWith() inside this fetch event handler?
    // This needs to be determined synchronously, which will give other fetch
    // handlers a chance to handle the request if need be.
    var shouldRespond;

    // First, remove all the ignored parameter and see if we have that URL
    // in our cache. If so, great! shouldRespond will be true.
    var url = stripIgnoredUrlParameters(event.request.url, ignoreUrlParametersMatching);
    shouldRespond = urlsToCacheKeys.has(url);

    // If shouldRespond is false, check again, this time with 'index.html'
    // (or whatever the directoryIndex option is set to) at the end.
    var directoryIndex = 'index.html';
    if (!shouldRespond && directoryIndex) {
      url = addDirectoryIndex(url, directoryIndex);
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond is still false, check to see if this is a navigation
    // request, and if so, whether the URL matches navigateFallbackWhitelist.
    var navigateFallback = '';
    if (!shouldRespond &&
        navigateFallback &&
        (event.request.mode === 'navigate') &&
        isPathWhitelisted([], event.request.url)) {
      url = new URL(navigateFallback, self.location).toString();
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond was set to true at any point, then call
    // event.respondWith(), using the appropriate cache key.
    if (shouldRespond) {
      event.respondWith(
        caches.open(cacheName).then(function(cache) {
          return cache.match(urlsToCacheKeys.get(url)).then(function(response) {
            if (response) {
              return response;
            }
            throw Error('The cached response that was expected is missing.');
          });
        }).catch(function(e) {
          // Fall back to just fetch()ing the request if some unexpected error
          // prevented the cached response from being valid.
          console.warn('Couldn\'t serve response for "%s" from cache: %O', event.request.url, e);
          return fetch(event.request);
        })
      );
    }
  }
});







