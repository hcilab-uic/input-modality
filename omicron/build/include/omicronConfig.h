/**************************************************************************************************
 * THE OMEGA LIB PROJECT
 *-------------------------------------------------------------------------------------------------
 * Copyright 2010-2011		Electronic Visualization Laboratory, University of Illinois at Chicago
 * Authors:										
 *  Alessandro Febretti		febret@gmail.com
 *-------------------------------------------------------------------------------------------------
 * Copyright (c) 2010-2011, Electronic Visualization Laboratory, University of Illinois at Chicago
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the documentation and/or other 
 * materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE  GOODS OR SERVICES; LOSS OF 
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************************/

 // omicron always builds as a shared library
#define OMICRON_SHARED
 
// WIN32 Platform-specific includes & macros.
#ifdef WIN32
	#ifdef OMICRON_SHARED
	#define WIN32_LEAN_AND_MEAN
	#ifdef OMICRON_EXPORTING
		#define OMICRON_API    __declspec(dllexport)
	#else
		#define OMICRON_API    __declspec(dllimport)
	#endif
	#else
		#define OMICRON_API
	#endif
#else
	#define OMICRON_API
#endif

// Operating system
#define OMICRON_OS_WIN
/* #undef OMICRON_OS_OSX */
/* #undef OMICRON_OS_LINUX */

// Architecture (32/64bit)
#define OMICRON_ARCH_32
/* #undef OMICRON_ARCH_64 */

// Build toolset
/* #undef OMICRON_TOOL_VS9 */
/* #undef OMICRON_TOOL_VS10 */
/* #undef OMICRON_TOOL_XCODE */
/* #undef OMICRON_TOOL_GCC */

// Enabled modules
#define OMICRON_USE_NETSERVICE
/* #undef OMICRON_USE_OPENNI */
#define OMICRON_USE_KINECT_FOR_WINDOWS
/* #undef OMICRON_USE_KINECT_FOR_WINDOWS_v1 */
#define OMICRON_USE_KINECT_FOR_WINDOWS_v2
/* #undef OMICRON_USE_KINECT_FOR_WINDOWS_AUDIO */
#define OMICRON_USE_NATURAL_POINT
/* #undef OMICRON_USE_OPTITRACK */
#define OMICRON_USE_PQLABS
#define OMICRON_USE_DIRECTINPUT
/* #undef OMICRON_USE_PSMOVEAPI */
#define OMICRON_USE_VRPN
/* #undef OMICRON_USE_THINKGEAR */
/* #undef OMICRON_USE_OSC */

#define OMICRON_DATA_PATH "C:/sage/omicron/source/data"
